#include "doubly_linked_list.h"
/*
 * You will be writing all of the code for each of these functions.
 * Remember, this is a doubly linked list, not an array. You need to
 * be using pointers, and not overwriting your values like you would
 * in an array.
 *
 * If you need to write auxiliary functions, you are more than welcome
 * to, but you can't change the signature of any of the functions we
 * have written.
 *
 * Information on doubly linked lists can be found at
 * https://en.wikipedia.org/wiki/Doubly_linked_list
 *
 * Hints: - Keep track of size. If you add or delete something, you
 *          need to change size.
 *        - This isn't an array, so moving things around is actually
 *          a lot easier. Just change the pointers to the objects.
 *        - Keep track of your edge cases; empty, 1 item, and 2 items
 *        - Some of these functions are basically the same thing,
 *          with the index shifted or return ignored. Don't rewrite
 *          code, just call the function with the 'correct' inputs.
 *        - Test your doubly linked list by itself before testing it
 *          in the deck class. It will make it easier to find any bugs
 *        - Use your debugger. It is your best friend for finding
 *          issues!
 *        - Don't forget to ask for help on Slack!
 *
 * We will be making changes throughout the week to the deck.cpp as
 * well as adding testing into the project. Make sure to pull and
 * merge frequently.
 */

namespace lab6 {
    // Default constructor
    doubly_linked_list::doubly_linked_list() {
        head = tail = nullptr;
        size = 0;

    }

    // Take in a vector of inputs and construct a doubly linked list from them
    doubly_linked_list::doubly_linked_list(std::vector<unsigned> values) {
        for(unsigned int i =0; i<values.size(); i++)
        {
            append(values[i]);
        }

    }

    // Copy constructor
    doubly_linked_list::doubly_linked_list(const doubly_linked_list &original) {
        doubly_linked_list copy_list;
        node *curr=original.head;
        while(curr!= nullptr)
        {
            copy_list.append(curr->data);
            curr=curr->next;
        }

    }
    // Create doubly linked linked list with one input value
    doubly_linked_list::doubly_linked_list(unsigned input) {
        if (head == nullptr) {
            node *temp = new node(input);
            head = temp;
            tail = temp;
            temp->data = input;
            temp->next = nullptr;
            temp->prev = nullptr;
        }
        size++;

    }

    // Default constructor
    doubly_linked_list::~doubly_linked_list() {
        node *curr = head;
        node *after_curr = nullptr;
        if (head == nullptr) {
            return;
        } else {
            while (curr != nullptr) {
                after_curr=curr->next;
                delete curr;
                curr=after_curr;
            }
        }
    }

    // return the value inside of the node located at position
    unsigned doubly_linked_list::get_data(unsigned position) {
        node *curr = head;
        int count = 0;
        if (head == nullptr) {
            exit(-1);
        }
        while (curr != nullptr) {
            if (count == position && position<size) {
                std::cout << curr->data << std::endl;
                return curr->data;
            }
            curr = curr->next;
            count++;
        }
    }

    // Get a set of values between position_from to position_to
    std::vector<unsigned> doubly_linked_list::get_set(unsigned position_from, unsigned position_to) {

        node *curr=head;
        if(head== nullptr)
        {
            exit(-1);
        }

    }

    // Add a value to the end of the list
    void doubly_linked_list::append(unsigned data) {
        node *tmp = new node(data);
        tmp->data = data;
        node *curr = head;
        if(head == nullptr &&size ==0)
        {
            head = tmp;
            tail = tmp;
            size++;
            return;
        }
        if(size==1)
        {
            curr->next=tmp;
            tmp->prev=curr;
            tail=tmp;
            size++;
            return;

        }
        if(size==2)
        {
            tail->next=tmp;
            tmp->prev=tail;
            tail=tmp;
            size++;
            return;
        }
        else {
            while (curr ->next != nullptr) {
                curr = curr->next;
            }
            curr->next = tmp;
            tmp->prev = tail;
            tail = tmp;
            size++;
        }
    }

    // Merge two lists together in place, placing the input list at the end of this list
    void doubly_linked_list::merge(doubly_linked_list &rhs) {

        doubly_linked_list copy_rhs;
        node*curr=rhs.head;
        while(curr!= nullptr) {
            copy_rhs.append(curr->data);
            curr=curr->next;
        }
        this->size=copy_rhs.size+this->size;
        this->tail->next=copy_rhs.head;
        this->tail=copy_rhs.tail;

    }

    // Allow for the merging of two lists using the + operator.
    doubly_linked_list doubly_linked_list::operator+(const doubly_linked_list &rhs) const {
        //    return ;
    }

    // Insert a node before the node located at position
    void doubly_linked_list::insert_before(unsigned position, unsigned data) {
        node *curr = head;
        node *trail_curr = nullptr;
        int count = 0;
        node *temp = new node(data);
        temp->data = data;
        if (head == nullptr) {
            head = temp;
            tail = temp;
            size = 1;
            return;
        } else {
            while (curr != nullptr) {
                if (position == 0 && size == 1) {
                    temp->next = curr;
                    head = temp;
                    curr->prev = temp;
                    tail = curr;
                    curr = head;
                    break;
                } else if (position == 0 && size == 2) {
                    temp->next = curr;
                    curr->prev = temp;
                    head = temp;
                    break;
                } else if (count == position && count > 0 && size == 2) {
                    temp->next = curr;
                    temp->prev = trail_curr;
                    curr->prev = temp;
                    trail_curr->next = temp;
                    tail = curr;
                    break;
                } else if (count == position) {
                    if (position == 0) {
                        temp->next = curr;
                        curr->prev = temp;
                        head = temp;
                        tail = temp;
                    } else {
                        temp->next = curr;
                        temp->prev = trail_curr;
                        trail_curr->next = temp;
                        curr->prev = temp;
                        tail = curr;
                    }
                }
                trail_curr = curr;
                curr = curr->next;
                count++;
            }
            size++;
        }
    }

    // Insert a node after the node located at position
    void doubly_linked_list::insert_after(unsigned position, unsigned data) {
        node *curr = head;
        node *temp = new node(data);
        temp->data = data;
        int count = 0;
        node *trail_curr = nullptr;
        while (curr != nullptr) {

            if (position == 0 && size == 1) {
                curr->next = temp;
                temp->prev = curr;
                tail = temp;
                break;
            } else if (position == 0 && size == 2) {
                trail_curr = curr;
                curr = curr->next;
                temp->prev = trail_curr;
                temp->next = curr;
                curr->prev = temp;
                trail_curr->next = temp;
                tail = curr;
                break;
            } else if (count == position && count > 0 && size == 2) {
                temp->prev = curr;
                curr->next = temp;
                tail = temp;
                break;
            } else if (count == position)
            {
                if(position==0)
                {
                    trail_curr=curr;
                    curr=curr->next;
                    temp->next=curr;
                    temp->prev=trail_curr;
                    trail_curr->next=temp;
                    curr->prev=temp;
                    tail=curr;
                }
                else if(position>1 && count>1)
                {
                    trail_curr->next=temp;
                    temp->prev=trail_curr;
                    temp->next=curr;
                    curr->prev=temp;
                    tail=curr;
                }
            }

        trail_curr = curr;
        curr = curr->next;
        count++;
    }
    size++;
}
    // Remove the node located at position from the linked list
    void doubly_linked_list::remove(unsigned position) {
        node *curr = head;
        node *trail_curr= nullptr;
        node *to_remove= nullptr;
        int count=0;

        if(head== nullptr)
        {
            size=0;
        }
        else if(position==0&&size==1)
        {
            head=tail=nullptr;
            delete curr;
            size=0;
        }
        else if(position==0 && size==2)
        {
            head=curr->next;
            tail=head;
            delete curr;
        }
        else if(position==1 && size==2)
        {
            curr=curr->next;
            delete curr;
            tail=head;
        }
        else {
            while (curr != nullptr) {
                if(position==0)
                {
                    head=curr->next;
                    delete curr;
                    break;
                }
                if(curr==tail) //delete last node.
                {
                    delete curr;
                    trail_curr->next= nullptr;
                    tail=trail_curr;
                    break;
                }
                else
                {
                    if(count==position)
                    {
                        trail_curr->next=tail;
                        tail->prev=trail_curr;
                        delete curr;
                        break;
                    }

                }
                trail_curr=curr;
                curr=curr->next;
                count++;
            }
        }
        size--;
    }

    // Split the list with the node being split on being included in the returned list
    doubly_linked_list doubly_linked_list::split_before(unsigned position) {
        //    return ;
    }

    // Split the list with the node being split on being included in the retained list
    doubly_linked_list doubly_linked_list::split_after(unsigned position) {
        //    return ;
    }

    // Create two lists, one starting at position_from and ending with position_to and return that list
    // Merge the beginning of the original list with the end of the original list and retain it
    doubly_linked_list doubly_linked_list::split_set(unsigned position_from, unsigned position_to) {
        //    return ;
    }

    // Swap two nodes in the list. USE POINTERS. Do not just swap the values!
    void doubly_linked_list::swap(unsigned position1, unsigned position2) {

    }

    // Swap two sets of cards. The sets are inclusive. USE POINTERS!
    void doubly_linked_list::swap_set(unsigned position1_from, unsigned position1_to, unsigned position2_from,
                                      unsigned position2_to) {

    }

    // Overload operator=
    doubly_linked_list &doubly_linked_list::operator=(const doubly_linked_list &RHS) {
        //    return <#initializer#>;
    }

    // Append the rhs to the end of the this list
    doubly_linked_list &doubly_linked_list::operator+=(const doubly_linked_list &RHS) {
        //    return <#initializer#>;
    }

    unsigned doubly_linked_list::get_size() {
        return size;
    }

    bool doubly_linked_list::is_empty() {
        if(head== nullptr)
        {
            return true;
        }

    }

    bool doubly_linked_list::operator==(const doubly_linked_list &rhs) const {
        node *iterL = head, *iterR = rhs.head;
        while (iterL != nullptr && iterR != nullptr) {
            if (iterL->data != iterR->data)
                return false;
            iterL = iterL->next;
            iterR = iterR->next;
        }
        return iterL == nullptr && iterR == nullptr;
    }

    std::string doubly_linked_list::to_string() {
        if (!head) return "";
        else {
            std::string output = "";
            output += std::to_string(head->data);
            node *to_return = head->next;
            while (to_return) {
                output += ", ";
                output += std::to_string(to_return->data);
                to_return = to_return->next;
            }
            return output;
        }
    }
}