#include "../inc/expression.h"
namespace lab5 {
    /****Auxillary Function prototypes ****/
    bool is_number(std::string input_string);

    bool is_operator(std::string input_string);

    int get_number(std::string input_string);

    std::string get_operator(std::string input_string);

    int operator_priority(std::string operator_in);

    /****end function prototypes****/


    expression::expression() {

    }

    expression::expression(std::string &input_expression) {
        queue q1;
        for(int i=0; i<input_expression.length(); i++)
        {
            q1.enqueue(input_expression);
        }

    }

    void expression::convert_to_postfix(std::string &input_expression) {
        stack s1;
        std::string postfix;
        for(int i=0; i<input_expression.length(); i++)
        {
            if(is_number(input_expression))
            {
                postfix[i]=input_expression[i];
            }
            if((is_operator(input_expression))&&s1.isEmpty())
            {
                s1.push(input_expression);
            }
            if(is_number(input_expression)&& !s1.isEmpty())
            {
                
            }
        }

    }

    void expression::parse_to_infix(std::string &input_expression) {

    }

    int expression::calculate_postfix() {
        return 0;
    }

    void expression::print_infix() {

    }

    void expression::print_postfix() {

    }

    std::istream &operator>>(std::istream &steam, expression &RHS) {
//    return <#initializer#>;
    }


//auxillary functions

    bool is_number(std::string input_string) {
        std::string num[]= {"1","2","3","4","5","6","7","8","9","0"};
        for(int i=0;i<10;i++)
        {
            if(input_string==num[i])
            {
                return true;
            }
        }
        return false;

    }

    bool is_operator(std::string input_string) {
        std::string ops[] = {"-", "+", "*", "/"};
        for(int i=0; i<4; i++)
        {
            if(input_string==ops[i])
            {
                return true;
            }
        }
    return false;
    }

    int get_number(std::string input_string) {

    }

    std::string get_operator(std::string input_string) {

    }

    int operator_priority(std::string operator_in) {
        if(operator_in == "*" || operator_in=="/")
        {
            return 2;
        }
        else if(operator_in=="+" || operator_in == "-")
        {
            return 1;
        }
        return 0;

    }
}