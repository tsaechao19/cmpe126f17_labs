#include "tree.h"
#include <iostream>
namespace lab7 {
    void clear(node *to_clear);

    void node_print_gtl(node *to_print);

    // Construct an empty tree
    tree::tree() {
        root = nullptr;
    }

    // Deconstruct tree
    tree::~tree() {
        clear(root);
    }

    // Insert
    void tree::insert(int value) {
        node *curr=root;
        node*trail_curr= nullptr;
        node *n = new node(value);
        n->data=value;
        if(curr == nullptr)
        {
            root=n;
            return;
        }
        while(curr!= nullptr)
        {
            trail_curr=curr;
            if(curr->data==value)
            {
                return;  //write code later, need to update frequency
            }
            else if(curr->data>value)
            {
                curr=curr->left;
            }
            else//(curr->data<value)
            {
                curr=curr->right;
            }
        }
        if(trail_curr->data>value)
        {
            trail_curr->left=n;
            return;
        }
        else if(trail_curr->data<value)
        {
            trail_curr->right=n;
            return;
        }

    }
    node* tree::remove_node(int key, node *n)
    {
        /*
        node *curr=root;
        if(root== nullptr)
        {
            return root;
        }
        if(curr->data>key)
        {
            curr->left=remove_node(key, curr->left);
        }
        else if(curr->data<key)
        {
            curr->right=remove_node(key,curr->right);
        }
        else
        {
            if(curr->left== nullptr)
            {
                node* temp = curr->right;
                delete curr;
                return temp;
            }
            else if(curr->right== nullptr)
            {
                node *temp=curr->left;
                delete curr;
                return temp;
            }
        }
*/
    }
    // Remove key
    bool tree::remove(int key) {
        node *curr= root;
        node *trail_curr= nullptr;
        if(root == nullptr)
        {
            exit(-1);
        }
        else
        {
            remove_node(key, curr);
        }
            /*
            else
            {
                trail_curr=root;
                while(curr!= nullptr)
                {
                    if(curr->data==key && curr->right== nullptr && curr->left== nullptr)
                    {
                        delete curr;
                        root= nullptr;
                        exit(-1);
                    }
                    if(curr->data==key)
                    {

                    }
                    if(curr->data>key)
                    {
                        curr=curr->left;
                    }
                    if(curr->data<key)
                    {
                        curr=curr->right;
                    }
                }
            }
*/
        }

    // What level is key on?
    int tree::level(int key) {

    }

    // Print the path to the key, starting with root
    void tree::path_to(int key) {

    }

    // Number of items in the tree
    unsigned tree::size() {
        return size();
    }

    // Calculate the depth of the tree, longest string of connections
    unsigned tree::depth() {

    }

    // Determine whether the given key is in the tree
    bool tree::in_tree(int key) {
        node *curr= nullptr;
        if(root== nullptr)
        {
            throw "list is empty";
        }

        else
        {
            curr=root;

            while(curr!= nullptr)
            {
                if(curr->data==key)
                {
                    std::cout<<key;
                    return key;

                }
                else if(curr->data>key)
                {
                    curr=curr->left;
                }
                else
                {
                    curr=curr->right;

                }
            }
            return key;
        }

    }

    // Return the number of times that value is in the tree
    int tree::get_frequency(int key) {

    }
    node* tree::search_tree(int key, node *n)
    {
        if(root->data==key || root== nullptr)
        {
            return root;
        }
        if(root->data>key)
        {
            return(key, root->left);
        }
        if(root->data<key)
        {
            return(key, root->right);
        }
    }

    // Print the tree least to greatest, Include duplicates
    void tree::print() {
    }

    void tree::print_gtl() {
        node_print_gtl(root);
        std::cout << std::endl;
    }

    void tree::print_rec(node*n)
    {

    }


    void node_print_gtl(node *top) {
        if(top == nullptr)
            return;
        node_print_gtl(top->right);
        for(int i = 0; i < top->frequency; i++)
            std::cout << top->data << " ";
        node_print_gtl(top->left);
        }

    void clear(node *to_clear) {
        if (to_clear == nullptr) return;
        if (to_clear->left != nullptr) clear(to_clear->left);
        if (to_clear->right != nullptr) clear(to_clear->right);
        delete to_clear;
    }

}