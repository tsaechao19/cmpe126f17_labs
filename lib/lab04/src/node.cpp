#include "node.h"
#include <iostream>
namespace lab4 {
// Take in value and create a node
    node::node(int input) {
        node *n;
        n = this;
        n->data = input;
        n->next = nullptr;
    }

// Takes in an array of values and creates the appropriate nodes
    node::node(int values[], int length) {
        this->data = values[0];
        node *temp_head = this;
        for (int i = 1; i < length; i++) {
            node *n = new node(values[i]);
            temp_head->next = n;
            temp_head = n;
        }
    }

// Default destructor
    node::~node() {
        // Hint: You don't want to just delete the current node. You need to keep track of what is next
    }

// Add a value to the end node
    void node::append(int input) {

        node *append_ptr = new node(input);
        append_ptr->data = input;
        append_ptr->next = nullptr;
        if (this == nullptr) {
            std::cout << "head is null";
        } else {
            node *n = this;
            while (n->next != nullptr) {
                n = n->next;
            }
            n->next = append_ptr;
        }

    }

// Add an array of values to the end as separate nodes
    void node::append(int inputs[], int length) {
        for (int i = 0; i < length; i++) {
            append(inputs[i]);
        }
    }

// Insert a new node after the given location
    node *node::insert(int location, int value) {
        node *current;
        node *n = new node(value);
        current = this;
        for (int i = 0; i < location; i++) {
            current = current->next;
        }
        if (current != nullptr) {
            n->next = current->next;
            current->next = n;
            return this;
        }

    }

// Remove a node and link the next node to the previous node
    node *node::remove(int location) {
        node *current = this;
        node *temp_prev = this;
        node *prev = this;
        node *after;
        int count = 0;
        if (location == 0) {
            node *n = this->next;
            delete current;
            return n;
        } else if (temp_prev != nullptr) {
            for (int i = 0; i < location; i++) {
                prev = temp_prev;
                temp_prev = temp_prev->next;
            }
            {
                while (current != nullptr) {
                    if (count == location) {
                        after = current->next;
                        prev->next = after;
                        delete current;
                        return this;
                    }
                    count++;
                    current = current->next;
                }
            }
        }
    }


// Print all nodes
    void node::print() {
        node *temp_hd;
        temp_hd = this;
        while (temp_hd!= nullptr) {
            std::cout << temp_hd->data << " -> ";
            temp_hd = temp_hd->next;
            if(temp_hd== nullptr)
            {
                std::cout<<"null";
            }
        }
    }

//Print the middle node
    void node::print_middle() {
        // HINT: Use a runner to traverse through the linked list at two different rates, 1 node per step
        //       and two nodes per step. When the faster one reaches the end, the slow one should be
        //       pointing to the middle
        node *run1 = this;
        node *run2 = this;
        if (this != nullptr) {
            while (run2 != nullptr && run2->next != nullptr) {
                run2 = run2->next->next;
                run1 = run1->next;
            }
        }
        std::cout << (run1->data);
    }

// Get the value of a given node
    int node::get_value(int location) {
        node *n = this;
        int count = 0;
        while (n != nullptr) {
            if (count == location) {
                std::cout << n->data;
            }
            count++;
            n = n->next;
        }

    }

// Overwrite the value of a given node
    void node::set_data(int location, int value) {
        node *n = this;
        int count = 0;
        while (n != nullptr) {
            if (count == location) {
                n->data = value;
            }
            count++;
            n = n->next;
        }

    }
}